import React from 'react';
import { SafeAreaView } from 'react-native';
import Feed from './src/components/Feed';
import FeedDetail from './src/components/FeedDetail';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Feed" component={Feed} options={{ title: 'Home' }} />
            <Stack.Screen name="FeedDetail" component={FeedDetail} options={{ title: 'Detail' }} />
          </Stack.Navigator>
        </NavigationContainer>
    </SafeAreaView>
  );
};


export default App;
