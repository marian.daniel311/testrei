import React, { useState } from 'react';
import { View, Text, TouchableOpacity, FlatList} from 'react-native';
import { styles } from '../styles';

const Feed = ({ navigation }) => {
    const [ isError, setIsError ] = useState(false);
    const [ feed, setFeed ] = useState([]);

    const clearList = () => {
        setIsError(false);
        setFeed([]);
    };

    const getApiResponse = () => {
        setIsError(false);
        let feedList = [];
        const promises = [
            fetch('https://api.netbet.com/development/randomFeed?website=casino&lang=eu&device=desktop&source=list1'),
            fetch('https://api.netbet.com/development/randomFeed?website=casino&lang=eu&device=desktop&source=list2'),
            fetch('https://api.netbet.com/development/randomFeed?website=casino&lang=eu&device=desktop&source=list3')
        ];
        Promise.all(promises).then(function (responses) {
            return Promise.all(responses.map(function (response) {
                return response.json();
            }));
        }).then(function (data) {
            const validResponses = data.filter((res) => res.status == true);
            if(validResponses.length == 0){
                setIsError(true);
            }
            else{
                validResponses.map((feed) => {
                    feedList = feedList.concat(feed.data.items);
                });
                setFeed(feedList.sort((a,b) => (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0)));
            }
        }).catch((err) => {
            console.log(err);
            setIsError(true);
        });
    };

    const renderItem = ({ item }) => (
        <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('FeedDetail', {item: item})}>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.title}</Text>
            <Text>{item.artist}</Text>
            <Text style={{color: 'blue', fontSize : 13}}>see details</Text>
        </TouchableOpacity>
    );

    return(
        <>
        <View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={[styles.button, {backgroundColor: "green"}]} onPress={getApiResponse}>
                    <Text style={[styles.buttonText, {color: '#fff'}]}>Ged Feed</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.button, {backgroundColor: "red"}]} onPress={clearList}>
                    <Text style={styles.buttonText}>Clear list</Text>
                </TouchableOpacity>
            </View>
            {isError && <Text style={{color:"red",paddingHorizontal: 20}}>We`ve encountered an error. Please try again.</Text>}
        </View>
        {feed.length == 0 && !isError ? <Text style={{color:"red",paddingHorizontal: 20}}>Feed is empty.</Text>
            :
            <FlatList
                style={{paddingHorizontal:20, paddingVertical: 10}}
                data={feed}
                renderItem={renderItem}
                keyExtractor={item => item.title.concat(item.artist)}
            /> 
            }
        </>
    )
}

export default Feed;