import React, { useState } from 'react';
import { View, Text, TouchableOpacity, FlatList} from 'react-native';
import { styles } from '../styles';

const FeedDetail = ({ route }) => {

    const { item } = route.params;
    return(
        <View style={{padding: 20}}>
            <Text style={{fontWeight: 'bold', fontSize: 20, marginBottom: 5}}>{item.title}</Text>
            <Text style={{fontSize: 17, marginBottom: 10}}>{item.artist}</Text>
            <Text style={{marginBottom:5}}>{item.label}, {item.year}</Text>
        </View>
    )
}

export default FeedDetail;