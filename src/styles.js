import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    button:{
        width:'49%',
        paddingVertical:10,
        borderRadius: 10,
        alignItems:'center',
    },
    buttonText:{
        fontWeight: 'bold',
    },
    buttonContainer:{
        flexDirection:'row',
        paddingVertical:30,
        paddingHorizontal: 20,
        justifyContent:'space-between'
    },
    card:{
        padding: 20,
        borderWidth:1,
        borderColor: 'grey',
        borderRadius: 5,
        marginBottom: 10,
    }
});